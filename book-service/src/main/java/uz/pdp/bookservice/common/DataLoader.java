package uz.pdp.bookservice.common;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import uz.pdp.bookservice.entity.Attachment;
import uz.pdp.bookservice.entity.Author;
import uz.pdp.bookservice.entity.Book;
import uz.pdp.bookservice.repository.AttachmentRepository;
import uz.pdp.bookservice.repository.AuthorRepository;
import uz.pdp.bookservice.repository.BookRepository;

import java.util.ArrayList;
import java.util.List;

// Nurkulov Nodirbek 4/24/2022  1:52 PM
@Component
public class DataLoader implements CommandLineRunner {

    @Value("${spring.sql.init.mode}")
    String initMode;

    final BookRepository bookRepository;
    final AuthorRepository authorRepository;
    final AttachmentRepository attachmentRepository;

    public DataLoader(BookRepository bookRepository, AuthorRepository authorRepository, AttachmentRepository attachmentRepository) {
        this.bookRepository = bookRepository;
        this.authorRepository = authorRepository;
        this.attachmentRepository = attachmentRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        if (initMode.equals("always")) {

            Attachment saveAttachment1 = attachmentRepository.save(new Attachment(
                    null,
                    null));

            //SAVING AUTHORS
            List<Author> authorList = new ArrayList<>();
            Author savedAuthor1 = authorRepository.save(new Author(
                    "Tohir Malik",
                    "Zor yozuvchi"));
            Author savedAuthor2 = authorRepository.save(new Author(
                    "Otkir Xoshimov",
                    "Zor shoir"));
            Author savedAuthor3 = authorRepository.save(new Author(
                    "Jonatan Swift",
                    "chet el shoiri"));

            authorList.add(savedAuthor1);
            authorList.add(savedAuthor2);
            authorList.add(savedAuthor3);


            //SAVING BOOKS
            bookRepository.save(new Book(
                    "Muvaffaqiyatli insonlarning 7 ko'nikmasi",
                    "Zor kitob",
                    authorList,
                    saveAttachment1
            ));
            bookRepository.save(new Book(
                    "Otamdan qolgan dalalar",
                    "Qiziqarli kitob",
                    authorList,
                    saveAttachment1
            ));
            bookRepository.save(new Book(
                    "Kichkina Shaxzoda",
                    "O'zbekcha kitob",
                    authorList,
                    saveAttachment1
            ));
        }

    }
}
