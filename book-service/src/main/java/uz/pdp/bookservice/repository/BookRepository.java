package uz.pdp.bookservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.bookservice.entity.Book;
import uz.pdp.clients.book.BookDto;

public interface BookRepository extends JpaRepository<Book,Integer> {
    Boolean existsByTitle(String title);

    @Query(nativeQuery = true,value = "")
    BookDto getBookById(Integer id);
}
