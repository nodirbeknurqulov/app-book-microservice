package uz.pdp.bookservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.bookservice.entity.Attachment;

public interface AttachmentRepository extends JpaRepository<Attachment,Integer> {
}
