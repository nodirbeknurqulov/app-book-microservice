package uz.pdp.bookservice.entity;

import lombok.*;
import lombok.experimental.PackagePrivate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

// Nurkulov Nodirbek 4/23/2022  12:11 PM
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity(name = "authors")
@PackagePrivate
public class Author {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    String fullName;

    String bio;

    public Author(String fullName, String bio) {
        this.fullName = fullName;
        this.bio = bio;
    }
}
