package uz.pdp.bookservice.entity;

import lombok.*;
import lombok.experimental.PackagePrivate;

import javax.persistence.*;
import java.util.List;

// Nurkulov Nodirbek 4/23/2022  8:37 AM
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@PackagePrivate
@Entity(name = "books")
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    String title;

    String description;

    @ManyToMany(mappedBy = "au")
    List<Author> authorList;

    @OneToOne
    Attachment attachment;

    public Book(String title, String description, List<Author> authorList, Attachment attachment) {
        this.title = title;
        this.description = description;
        this.authorList = authorList;
        this.attachment = attachment;
    }
}
