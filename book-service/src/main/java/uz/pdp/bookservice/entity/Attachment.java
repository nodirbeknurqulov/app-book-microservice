package uz.pdp.bookservice.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "attachments")
@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
public class Attachment{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    String contentType;
    Long size;

    public Attachment(String contentType, Long size) {
        this.contentType = contentType;
        this.size = size;
    }
}
