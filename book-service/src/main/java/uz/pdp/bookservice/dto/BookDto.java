package uz.pdp.bookservice.dto;
// Nurkulov Nodirbek 4/24/2022  1:56 PM

import lombok.Data;
import lombok.experimental.PackagePrivate;

@PackagePrivate
@Data
public class BookDto {
    String title;

    String description;

    Integer authorId;
}
