package uz.pdp.bookservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import uz.pdp.bookservice.common.ApiResponse;
import uz.pdp.bookservice.dto.BookDto;
import uz.pdp.bookservice.entity.Author;
import uz.pdp.bookservice.entity.Book;
import uz.pdp.bookservice.repository.AuthorRepository;
import uz.pdp.bookservice.repository.BookRepository;
import uz.pdp.clients.book.BookClient;

import java.util.List;
import java.util.Optional;

// Nurkulov Nodirbek 4/8/2022  10:57 AM
@Service
@RequiredArgsConstructor
public class BookService {

    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;
    private final BookClient bookClient;

    /**
     * GET ALL BOOKS BY PAGEABLE
     *
     * @param page int
     * @param size int
     * @return HttpEntity
     */
    public List<Book> getAllBooks(int page, int size) {

        PageRequest pageRequest = PageRequest.of(page, size);
        Page<Book> bookPage = bookRepository.findAll(pageRequest);
        return bookPage.getContent();
    }


    /**
     * GET BOOK BY ID
     *
     * @param id Integer
     * @return HttpEntity
     */
    public uz.pdp.clients.book.BookDto  getBookById(Integer id) {
        uz.pdp.clients.book.BookDto  optionalBook = bookRepository.getBookById(id);
        return optionalBook;
    }

    /**
     * ADD BOOK
     *
     * @param bookDto BookDto
     * @return ApiResponse
     */
//    public ApiResponse addBook(BookDto bookDto) {
//        boolean existsByTitle = bookRepository.existsByTitle(bookDto.getTitle());
//        if (existsByTitle) {
//            return new ApiResponse("This book is already exist");
//        }
//
//        Book newBook = new Book();
//        newBook.setTitle(bookDto.getTitle());
//        newBook.setDescription(bookDto.getDescription());
//
//        Author newAuthor = new Author();
//        newBook.setAuthor(newAuthor);
//        authorRepository.save(newAuthor);
//
//        Book savedBook = bookRepository.save(newBook);
//
//        return new ApiResponse("Book added!", true, savedBook);
//    }

    /**
     * EDIT BOOK
     *
     * @param bookDto BookDto
     * @param id      Integer
     * @return AptResponse
     */
//    public ApiResponse editBookById(BookDto bookDto, Integer id) {
//        Optional<Book> optionalBook = bookRepository.findById(id);
//        if (!optionalBook.isPresent()) {
//            return new ApiResponse("Book not found for editing!", false);
//        }
//        Book editingBook = optionalBook.get();
//        editingBook.setTitle(bookDto.getTitle());
//        editingBook.setDescription(bookDto.getDescription());
//
//        Optional<Author> optionalAuthor = authorRepository.findById(id);
//        if (!optionalAuthor.isPresent()) {
//            return new ApiResponse("Actor not found for editing!", false);
//        }
//        Author editingActor = optionalAuthor.get();
//        editingBook.setAuthor(editingActor);
//        bookRepository.save(editingBook);
//        return new ApiResponse("Actor edited!", true, editingBook);
//    }

    /**
     * DELETE BOOK BY ID
     *
     * @param id Integer
     * @return ApiResponse
     */
    public ApiResponse deleteBookById(Integer id) {
        Optional<Book> optionalBook = bookRepository.findById(id);
        if (!optionalBook.isPresent()) {
            return new ApiResponse("Book not found!", false);
        }
        bookRepository.deleteById(id);
        return new ApiResponse("Book deleted!", true);
    }
}
