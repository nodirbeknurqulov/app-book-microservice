package uz.pdp.bookservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.bookservice.common.ApiResponse;
import uz.pdp.bookservice.dto.BookDto;
import uz.pdp.bookservice.entity.Book;
import uz.pdp.bookservice.service.BookService;

import java.util.List;

@RestController
@RequestMapping("/api/book")
@RequiredArgsConstructor
public class BookController {

    private final BookService bookService;

    /**
     * GET ALL BOOKS BY PAGEABLE
     *
     * @param page int
     * @param size int
     * @return HttpEntity
     */
    @GetMapping
    public HttpEntity<?> getAllBooks(@RequestParam(defaultValue = "0") int page,
                                     @RequestParam(defaultValue = "10") int size) {
        List<Book> allBooks = bookService.getAllBooks(page, size);
        return ResponseEntity.ok(allBooks);
    }

    /**
     * GET BOOK BY ID
     *
     * @param id Integer
     * @return HttpEntity
     */
    @GetMapping("/{id}")
    public uz.pdp.clients.book.BookDto  getBookById(@PathVariable Integer id) {
        return bookService.getBookById(id);
    }

    /**
     * ADD BOOK
     *
     * @param bookDto BookDto
     * @return ApiResponse
     */
//    @PostMapping
//    public ApiResponse addBook(@RequestBody BookDto bookDto) {
//        ApiResponse addedBook = bookService.addBook(bookDto);
//        return new ApiResponse(addedBook);
//    }

    /**
     * EDIT BOOK
     * @param bookDto BookDto
     * @param id Integer
     * @return AptResponse
     */
//    @PutMapping("/{id}")
//    public ApiResponse editBook(@RequestBody BookDto bookDto, @PathVariable Integer id) {
//        ApiResponse editBook = bookService.editBookById(bookDto, id);
//        return new ApiResponse("Book edited!", true, editBook);
//    }

    /**
     * DELETE BOOK BY ID
     * @param id Integer
     * @return ApiResponse
     */
    @DeleteMapping("/{id}")
    public ApiResponse deleteBookById(@PathVariable Integer id){
        ApiResponse apiResponse = bookService.deleteBookById(id);
        return new ApiResponse(true,apiResponse);
    }
}
