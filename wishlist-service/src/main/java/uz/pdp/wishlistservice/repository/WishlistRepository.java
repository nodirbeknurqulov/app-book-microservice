package uz.pdp.wishlistservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.wishlistservice.entity.Wishlist;

public interface WishlistRepository extends JpaRepository<Wishlist,Integer> {
}
