package uz.pdp.wishlistservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import uz.pdp.wishlistservice.common.ApiResponse;
import uz.pdp.wishlistservice.dto.BookDto;
import uz.pdp.wishlistservice.dto.WishlistDto;
import uz.pdp.wishlistservice.entity.Wishlist;
import uz.pdp.wishlistservice.repository.WishlistRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class WishlistService {

    @Autowired
    WishlistRepository wishlistRepository;

    public HttpEntity<?> getAllWishlist() {
        try {
            List<BookDto>bookDtoList=new ArrayList<>();
            for (Wishlist wishlist : wishlistRepository.findAll()) {
                Integer bookId=wishlist.getBookId();

                RestTemplate restTemplate=new RestTemplate();

                BookDto forObject = restTemplate.getForObject("htts://book/api/book/getById/" + bookId, BookDto.class);
                bookDtoList.add(forObject);
            }

            return new ResponseEntity<>(new ApiResponse("Success", true, bookDtoList), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(new ApiResponse("wrong", false, false), HttpStatus.BAD_REQUEST);

        }
    }

    public HttpEntity<?> saveWishlist(WishlistDto wishlistDto) {
        try {
            Wishlist wishlist = new Wishlist();
            wishlist.setBookId(wishlistDto.getBookId());
            wishlist.setUserId(wishlistDto.getUserId());
            wishlistRepository.save(wishlist);
            return new ResponseEntity<>(new ApiResponse("Success", true, "save"), HttpStatus.OK);

        }catch (Exception e){
            return new ResponseEntity<>(new ApiResponse("wrong", false, false), HttpStatus.BAD_REQUEST);

        }
    }

    public HttpEntity<?> deleteByIdWishlist(Integer bookId) {

        try {
            wishlistRepository.deleteById(bookId);
            return new ResponseEntity<>(new ApiResponse("Success", true, "delete"), HttpStatus.OK);

        }catch (Exception e){
            return new ResponseEntity<>(new ApiResponse("wrong", false, false), HttpStatus.BAD_REQUEST);

        }
    }
}
