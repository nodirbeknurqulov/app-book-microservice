package uz.pdp.wishlistservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uz.pdp.wishlistservice.dto.WishlistDto;
import uz.pdp.wishlistservice.service.WishlistService;

@Controller
@RequestMapping("/api/wishlist")
public class WishlistController {

    @Autowired
    WishlistService wishlistService;

    @GetMapping()
    public HttpEntity<?> getAllWishlist() {
        return wishlistService.getAllWishlist();
    }

    @PostMapping
    public HttpEntity<?> saveWishlist(@RequestBody WishlistDto wishlistDto) {
        return wishlistService.saveWishlist(wishlistDto);
    }

    @DeleteMapping("/{bookId}")
    public HttpEntity<?> deleteByIdWishlist(@PathVariable Integer bookId) {
        return wishlistService.deleteByIdWishlist(bookId);
    }
}
