package uz.pdp.clients.book;

import org.springframework.cloud.loadbalancer.annotation.LoadBalancerClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import uz.pdp.clients.config.LoadBalancerConfiguration;

@FeignClient("book")
@LoadBalancerClient(name = "book", configuration = LoadBalancerConfiguration.class)
public interface BookClient {

    @GetMapping("/api/book/{bookId}")
    BookDto getBookById(@PathVariable("bookId") Integer bookId);

}
