package uz.pdp.clients.book;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
public class BookDto {

    Integer id;

    Integer attachmentId;

    String title;

    String authorName;

    Integer authorId;

    int bookMark;
}
